
This is the code for the exercise asked by cbr genomics.

# Getting Started

To run it you'll first need a postgres database and java 8.
The database configuration is present in the pom.xml (properties tag)

## Trying out the application

To run the code please use the following command:

``
mvn clean install exec:java
``

It will:

*  Create the database schema and apply the flyway migrations

* Run the application with the default spring profile
 
I've also included for your convenience a swagger UI with the exposed endpoints, it can be accessed in http://localhost:8080/swagger-ui/

This swagger ui can be disabled by launching the application with the spring ``prod`` profile.

### Authentication

Also, I took the liberty to create the user "admin" (password: admin) that already has the ADMIN role.

Please note that the token generated is a bearer token, so, when configuring the Authentication in swagger don't forget that the Authorization header is in the form: 

``Authorization: Bearer <THE_GENERATED_TOKEN>``

# Design considerations

Regarding the main components of the application i used the following technologies:

*  Flyway: to initialize the database and version it

*  JOOQ: I prefer jooq to JPA since it gives us more flexibility and it offers better performance. Plus, JPA with Hibernate often are missused by the developers, that add too many eager loaded dependencies to each object, or load/update data that they end up not needing.
Another advantage is that jooq generates the DTOs based on the real database state. This way, if there is any DDL change we would know exacly where it impacts.

*  JWT: For authentication i use a signed jwt token. To check its validity I check the HMAC512 signature included in the token. No need to keep the session information on a database.






