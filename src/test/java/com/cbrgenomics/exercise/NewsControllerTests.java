package com.cbrgenomics.exercise;

import com.cbrgenomics.exercise.controllers.NewsController;
import com.cbrgenomics.exercise.controllers.dto.CreateNewsDTO;
import com.cbrgenomics.exercise.controllers.errorhandling.DuplicateUpvoteException;
import com.cbrgenomics.exercise.datasource.enums.Role;
import com.cbrgenomics.exercise.datasource.tables.pojos.News;
import com.cbrgenomics.exercise.datasource.tables.pojos.User;
import com.cbrgenomics.exercise.repositories.InteractionRepository;
import com.cbrgenomics.exercise.repositories.NewsRepository;
import com.cbrgenomics.exercise.repositories.OutboxRepository;
import com.cbrgenomics.exercise.repositories.UserRepository;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;

/**
 * This is just a sample test, didn't have time to create all the needed test cases
 */
@SpringBootTest(properties = { "cbrgenomics.upvotes.threshold=1" })
@WithMockUser(username="user")
public class NewsControllerTests {

    @MockBean
    private NewsRepository newsRepo;

    @MockBean
    private InteractionRepository interactionRepo;

    @MockBean
    private OutboxRepository outboxRepo;

    @MockBean
    private UserRepository userRepo;

    @Autowired
    private NewsController controller;

    @Test
    void testUpvoteNotification() throws DuplicateUpvoteException {
        Mockito.when(interactionRepo.interact(any(), any(), any())).thenReturn(1);
        Mockito.when(newsRepo.upvote(1L)).thenReturn(1);
        Mockito.when(newsRepo.find(1L)).thenReturn(new News().setPublished(true));

        controller.upvote(1L);

        Mockito.verify(outboxRepo, times(1)).sendUpvotesNotification(1L);
    }

    @Test
    void testUpdateNotification() {
        Mockito.when(newsRepo.find(1L)).thenReturn(new News().setCreatedBy(1L).setPublished(true));
        Mockito.when(userRepo.searchByUsername("user"))
                .thenReturn(Optional.of(new User().setRole(Role.USER).setId(1L).setUsername("user")));

        CreateNewsDTO dto = new CreateNewsDTO();
        dto.setBody("The body");
        dto.setTitle("The title");
        controller.update(dto, 1L);

        Mockito.verify(outboxRepo, times(1)).sendModificationNotification(1L);
    }

    @Test
    void testUpdateSecurityFail() {
        Mockito.when(newsRepo.find(1L)).thenReturn(new News().setCreatedBy(1L).setPublished(true));
        Mockito.when(userRepo.searchByUsername("user"))
                .thenReturn(Optional.of(new User().setRole(Role.ADMIN).setId(2L).setUsername("user")));

        CreateNewsDTO dto = new CreateNewsDTO();
        dto.setBody("The body");
        dto.setTitle("The title");

        assertThrows(AccessDeniedException.class, () -> controller.update(dto, 1L));
    }

}
