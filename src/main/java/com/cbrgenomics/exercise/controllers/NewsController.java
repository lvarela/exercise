package com.cbrgenomics.exercise.controllers;

import com.cbrgenomics.exercise.controllers.dto.CreateNewsDTO;
import com.cbrgenomics.exercise.controllers.dto.NewsDTO;
import com.cbrgenomics.exercise.controllers.dto.NewsTitleDTO;
import com.cbrgenomics.exercise.controllers.errorhandling.DuplicateUpvoteException;
import com.cbrgenomics.exercise.datasource.enums.InteractionType;
import com.cbrgenomics.exercise.datasource.enums.Role;
import com.cbrgenomics.exercise.datasource.tables.pojos.News;
import com.cbrgenomics.exercise.datasource.tables.pojos.User;
import com.cbrgenomics.exercise.repositories.InteractionRepository;
import com.cbrgenomics.exercise.repositories.NewsRepository;
import com.cbrgenomics.exercise.repositories.OutboxRepository;
import io.swagger.annotations.Api;
import org.apache.commons.lang3.StringUtils;
import org.jooq.exception.DataAccessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import javax.validation.constraints.*;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.cbrgenomics.exercise.datasource.Indexes.UIN_ONE_UPVOTE_PER_USR;

@Validated
@RestController
@RequestMapping(path = "news")
@Api("Here you can interact with the news services")
public class NewsController {

    @Autowired
    private NewsRepository newsRepo;

    @Autowired
    private InteractionRepository interactionRepo;

    @Autowired
    private OutboxRepository outbox;

    @Resource(name = "currentUser")
    private User currentUser;

    @Value("${cbrgenomics.upvotes.threshold}")
    private Integer threshold;

    @Value("${cbrgenomics.upvotes.limitOnePerUser}")
    private Boolean limitOneUpvotePerUser;

    private static final Logger LOG = LoggerFactory.getLogger(NewsController.class);

    /**
     * Creates a new news item
     * @param dto
     */
    @PostMapping("/")
    @PreAuthorize("hasRole('ROLE_USER')")
    @Transactional(rollbackFor = { Throwable.class })
    public void create(@RequestBody @Valid CreateNewsDTO dto) {
        newsRepo.insert(new News()
                .setCreatedAt(LocalDateTime.now())
                .setText(dto.getBody())
                .setPublished(false)
                .setCreatedBy(currentUser.getId())
                .setTitle(dto.getTitle())
        );
    }

    /**
     * List the news items, could use some pagination
     */
    @GetMapping("/")
    @PreAuthorize("hasRole('ROLE_USER')")
    @Transactional(rollbackFor = { Throwable.class }, readOnly = true)
    public ResponseEntity<List<NewsTitleDTO>> list() {
        //any user can see published news
        Stream<News> published = newsRepo.findByPublishStatus(true);

        Stream<News> unpublishedNews;
        switch (currentUser.getRole()) {
            case ADMIN:
                //admin user can see all unpublished news
                unpublishedNews = newsRepo.findByPublishStatus(false);
                break;
            default:
                //other users can only see their own unpublished news
                unpublishedNews = newsRepo.findUnpublishedByAuthor(currentUser.getId());
        }

        return ResponseEntity.ok().body(Stream.concat(published, unpublishedNews)
                .map(n -> new NewsTitleDTO(n.getId(), n.getTitle(), n.getPublished(), n.getCreatedBy(), n.getUpvotes()))
                .collect(Collectors.toList()));
    }

    /**
     * Updates the news
     * @param dto
     * @param id
     * @return
     */
    @PutMapping("/{id}")
    @PreAuthorize("hasRole('ROLE_USER') && hasPermission(#id, 'NEWS', 'WRITE')")
    @Transactional(rollbackFor = { Throwable.class })
    public ResponseEntity update(@RequestBody @Valid CreateNewsDTO dto,
                                 @Valid
                                 @PathVariable(value="id")
                                 @Min(message = "ID_STARTS_AT_ZERO", value = 0)
                                 @NotNull(message = "ID_MANDATORY") Long id) {

        News news = new News().setText(dto.getBody()).setTitle(dto.getTitle());

        newsRepo.update(news, id);

        if (newsRepo.find(id).getPublished()) {
            LOG.info("Launching notification for updating the news #{}, to all who has seen it", id);
            //if published, each time the news is updated, we launch an event
            outbox.sendModificationNotification(id);
        }

        return ResponseEntity.ok().build();
    }

    /**
     * Reads a single news item
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    @PreAuthorize("hasRole('ROLE_USER') && hasPermission(#id, 'NEWS', 'READ')")
    @Transactional(rollbackFor = { Throwable.class }, readOnly = true)
    public ResponseEntity<NewsDTO> read(@Valid @PathVariable(value="id")
                       @Min(message = "ID_STARTS_AT_ZERO", value = 0)
                       @NotNull(message = "ID_MANDATORY") Long id) {

        News news = newsRepo.find(id);

        return ResponseEntity.ok()
                .body(new NewsDTO(news.getId(), news.getTitle(), news.getText(), news.getPublished(), news.getCreatedBy(), news.getUpvotes()));
    }

    /**
     * Adds an upvote to the news
     *
     * We don't want for the user to wait for the notifications to be sent while he is upvoting,
     * instead we record it's upvote, and if it's the first time the user is upvoting, it will register
     * an event saying that we reached the threshold.
     * This way the action is:
     *  - Transactional
     *  - Asynchronous
     *
     * On the downside, we have a lock on the NEWS row, it will cause contention.
     * The alternative is query the interactions table and maintain a "materialized view" with the upvote count
     * and what if the notifications were sent for each of the news
     */
    @PutMapping("/upvote/{id}")
    @PreAuthorize("hasRole('ROLE_USER') && hasPermission(#id, 'NEWS', 'UPVOTE')")
    @Transactional(rollbackFor = { Throwable.class })
    public ResponseEntity upvote(@Valid @PathVariable(value="id")
                       @Min(message = "ID_STARTS_AT_ZERO", value = 0)
                       @NotNull(message = "ID_MANDATORY") Long id) throws DuplicateUpvoteException {
        try {
            if (!limitOneUpvotePerUser || interactionRepo.interact(currentUser.getId(), id, InteractionType.UPVOTE) > 0) {
                int currentUpvotes = newsRepo.upvote(id);

                //since we have a lock on the database, this is safe
                if (currentUpvotes == threshold) {
                    outbox.sendUpvotesNotification(id);
                }
            } else {
                //if the news is not published
                return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
            }
        } catch (DataAccessException ex) {
            if (StringUtils.containsIgnoreCase(ex.getMessage(), UIN_ONE_UPVOTE_PER_USR.getName())) {
                throw new DuplicateUpvoteException();
            }
            throw ex;
        }

        return ResponseEntity.ok().build();
    }

    /**
     * Informs the system that a user has visualized the news,
     * Useful to send modification notifications and analytics
     * @param id
     */
    @PutMapping("/visualize/{id}")
    @PreAuthorize("hasRole('ROLE_USER') && hasPermission(#id, 'NEWS', 'VISUALIZE')")
    @Transactional(rollbackFor = { Throwable.class })
    public void visualize( @Valid @PathVariable(value="id")
                       @Min(message = "ID_STARTS_AT_ZERO", value = 0)
                       @NotNull(message = "ID_MANDATORY") Long id) {
        interactionRepo.interact(currentUser.getId(), id, InteractionType.VISUALIZATION);
    }

    /**
     * Publish the news
     * @param id
     * @return
     */
    @PutMapping("/publish/{id}")
    @PreAuthorize("hasRole('ROLE_USER') && hasPermission(#id, 'NEWS', 'PUBLISH')")
    @Transactional(rollbackFor = { Throwable.class })
    public ResponseEntity publish(@Valid @PathVariable(value="id")
                       @Min(message = "ID_STARTS_AT_ZERO", value = 0)
                       @NotNull(message = "ID_MANDATORY") Long id) {

        newsRepo.publish(id);

        return ResponseEntity.ok().build();
    }
}
