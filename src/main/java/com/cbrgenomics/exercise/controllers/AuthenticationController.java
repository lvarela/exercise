package com.cbrgenomics.exercise.controllers;

import com.cbrgenomics.exercise.controllers.dto.AuthenticationDTO;
import com.cbrgenomics.exercise.controllers.dto.LoginResponseDTO;
import com.cbrgenomics.exercise.controllers.errorhandling.UniqueUsernameException;
import com.cbrgenomics.exercise.datasource.enums.NotificationType;
import com.cbrgenomics.exercise.datasource.enums.Role;
import com.cbrgenomics.exercise.datasource.tables.pojos.User;
import com.cbrgenomics.exercise.repositories.UserRepository;
import com.cbrgenomics.exercise.services.JwtUtils;
import io.swagger.annotations.Api;

import static org.apache.commons.lang3.StringUtils.*;
import org.jooq.exception.DataAccessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.security.PermitAll;
import javax.validation.Valid;
import java.time.LocalDateTime;

@Validated
@RestController
@Api("Authentication api")
public class AuthenticationController {

    @Autowired
    private UserRepository userRepo;

    @Autowired
    private PasswordEncoder encoder;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Value("${oauth.token.maxAge}")
    private Long maxAge;

    @Autowired
    private JwtUtils jwtUtil;

    private static final Logger LOG = LoggerFactory.getLogger(AuthenticationController.class);

    /**
     * Registers a new user
     * @param user
     */
    @PermitAll
    @PostMapping("register")
    @Transactional(rollbackFor = { Throwable.class })
    public void register(@RequestBody @Valid AuthenticationDTO user) throws UniqueUsernameException {
        try {
            userRepo.insert(new User()
                    .setCreatedAt(LocalDateTime.now())
                    .setUsername(user.getUsername())
                    .setPassword(encoder.encode(user.getPassword()))
                    .setNotificationType(NotificationType.SMOKE_SIGNALS) //the default option, could be null
                    .setRole(Role.USER));
        } catch (DataAccessException ex) {
            //check if it's failing because of unique constraint on username
            if (containsIgnoreCase(ex.getMessage(), "username")
                    && containsIgnoreCase(ex.getMessage(), "unique")) {
                throw new UniqueUsernameException();
            }
            throw ex;
        }
    }

    /**
     * Logs in a user, and generates a JWT token
     * @param login
     * @return
     */
    @PermitAll
    @PostMapping("authenticate")
    @Transactional(rollbackFor = { Throwable.class }, readOnly = true)
    public ResponseEntity<Object> authenticate(@RequestBody @Valid AuthenticationDTO login) {
        try {
            Authentication authenticate = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(login.getUsername(), login.getPassword()));

            UserDetails user = (UserDetails) authenticate.getPrincipal();

            return ResponseEntity.ok(new LoginResponseDTO(jwtUtil.getToken(user.getUsername()), maxAge));
        } catch (BadCredentialsException ex) {
            LOG.warn("Failed login attempt", ex);
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
    }

}
