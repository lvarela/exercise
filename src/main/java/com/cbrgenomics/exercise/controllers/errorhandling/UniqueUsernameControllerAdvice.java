package com.cbrgenomics.exercise.controllers.errorhandling;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.http.HttpStatus.BAD_REQUEST;

/**
 * This controller advice parses validation errors and returns them to the user.
 */
@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class UniqueUsernameControllerAdvice {

    @ResponseStatus(BAD_REQUEST)
    @ResponseBody
    @ExceptionHandler(UniqueUsernameException.class)
    public ResponseEntity<?> methodArgumentNotValidException(UniqueUsernameException ex) {
        List<ValidationError> error = new ArrayList<>();
        error.add(new ValidationError("username", "USERNAME_ALREADY_EXISTS"));
        return ResponseEntity.badRequest().body(error);
    }

}
