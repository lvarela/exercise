package com.cbrgenomics.exercise.controllers;

import com.cbrgenomics.exercise.controllers.dto.UpdateNotificationTypeDTO;
import com.cbrgenomics.exercise.datasource.tables.pojos.User;
import com.cbrgenomics.exercise.repositories.UserRepository;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.function.Supplier;

@Validated
@RestController
@RequestMapping(path = "user")
@Api("User api")
@PreAuthorize("hasRole('ROLE_USER')")
public class UserController {

    @Autowired
    private UserRepository userRepo;

    @Autowired
    private PasswordEncoder encoder;

    @Resource(name = "currentUser")
    private User currentUser;

    private static final Logger LOG = LoggerFactory.getLogger(UserController.class);

    @PutMapping("/")
    @Transactional(rollbackFor = { Throwable.class })
    public void update( @RequestBody @Valid UpdateNotificationTypeDTO newType) {
        userRepo.updateNotificationType(currentUser.getId(), newType.getType());
    }

}
