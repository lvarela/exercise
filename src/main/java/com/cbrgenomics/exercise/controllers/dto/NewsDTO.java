package com.cbrgenomics.exercise.controllers.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Getter
@Setter
public class NewsDTO {

    private Long id;

    private String title;

    private String body;

    private boolean isPublished;

    private Long createdBy;

    private Integer upvotes;

    public NewsDTO(Long id, String title, String body, boolean isPublished, Long createdBy, Integer upvotes) {
        this.id = id;
        this.title = title;
        this.body = body;
        this.isPublished = isPublished;
        this.createdBy = createdBy;
        this.upvotes = upvotes;
    }
}
