package com.cbrgenomics.exercise.controllers.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NewsTitleDTO {

    private Long id;

    private String title;

    private boolean isPublished;

    private Long createdBy;

    private Integer upvotes;

    public NewsTitleDTO(Long id, String title, boolean isPublished, Long createdBy, Integer upvotes) {
        this.id = id;
        this.title = title;
        this.isPublished = isPublished;
        this.createdBy = createdBy;
        this.upvotes = upvotes;
    }
}
