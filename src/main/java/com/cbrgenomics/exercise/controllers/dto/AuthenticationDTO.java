package com.cbrgenomics.exercise.controllers.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.*;

@Getter
@Setter
public class AuthenticationDTO {

    @Size(message = "USERNAME_LENGTH", max = 255, min = 1)
    @Pattern(message = "USERNAME_BAD_PATTERN", regexp = "^\\w+$")
    @NotBlank(message = "USERNAME_MANDATORY")
    private String username;

    @Size(message = "PASSWORD_LENGTH", max = 255, min = 5)
    @Pattern(message = "PASSWORD_HAS_SPACES", regexp = "^\\S+$")
    @NotBlank(message = "PASSWORD_MANDATORY")
    private String password;

}
