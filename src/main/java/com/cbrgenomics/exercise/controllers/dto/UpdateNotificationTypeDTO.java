package com.cbrgenomics.exercise.controllers.dto;

import com.cbrgenomics.exercise.datasource.enums.NotificationType;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class UpdateNotificationTypeDTO {

    @NotNull(message = "TYPE_IS_MANDATORY")
    private NotificationType type;

}
