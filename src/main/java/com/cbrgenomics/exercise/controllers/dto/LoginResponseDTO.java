package com.cbrgenomics.exercise.controllers.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LoginResponseDTO {

    private String token;

    private Long maxAge;

    public LoginResponseDTO(String token, Long maxAge) {
        this.token = token;
        this.maxAge = maxAge;
    }

}
