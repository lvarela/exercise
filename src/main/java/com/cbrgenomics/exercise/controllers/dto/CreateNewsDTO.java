package com.cbrgenomics.exercise.controllers.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Getter
@Setter
public class CreateNewsDTO {

    @Size(message = "TITLE_LENGTH", max = 255, min = 1)
    @NotBlank(message = "TITLE_MANDATORY")
    private String title;

    @Size(message = "BODY_LENGTH", max = 2000, min = 1)
    @NotBlank(message = "BODY_MANDATORY")
    private String body;

}
