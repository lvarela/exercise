package com.cbrgenomics.exercise.controllers;

import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.security.PermitAll;

@PermitAll
@RestController
@Api("Dummy controller for fake kubernetes endpoints")
public class DummyController {


    @GetMapping("/liveness")
    void isAlive() {
        //do nothing
    }

    @GetMapping("/readiness")
    void isReady() {
        //do nothing
    }

}
