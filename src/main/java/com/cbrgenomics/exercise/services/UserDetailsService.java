package com.cbrgenomics.exercise.services;

import com.cbrgenomics.exercise.controllers.AuthenticationController;
import com.cbrgenomics.exercise.datasource.enums.Role;
import com.cbrgenomics.exercise.repositories.UserRepository;
import org.jooq.DSLContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


@Service
public class UserDetailsService implements org.springframework.security.core.userdetails.UserDetailsService {

        @Autowired
        private UserRepository user;

        @Override
        @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
        public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
            return user.searchByUsername(username)
                    .map(u -> new CbrUserDetails(u.getUsername(), u.getPassword(), u.getRole()))
                    .orElseThrow(() -> new UsernameNotFoundException(username));
        }


        public static class CbrUserDetails implements UserDetails {
            private String password;
            private String username;
            private List<GrantedAuthority> authorities;

            public CbrUserDetails(String username, String password, Role role) {
                this.password = password;
                this.username = username;
                this.authorities = new ArrayList<>();

                //granted authorities must be prefixed with role if they want to be used as such
                this.authorities.add((GrantedAuthority) () -> "ROLE_" + role.name());
            }

            @Override
            public Collection<? extends GrantedAuthority> getAuthorities() {
                return new ArrayList<>(this.authorities);
            }

            @Override
            public String getPassword() {
                return password;
            }

            @Override
            public String getUsername() {
                return username;
            }

            @Override
            public boolean isAccountNonExpired() {
                return true;
            }

            @Override
            public boolean isAccountNonLocked() {
                return true;
            }

            @Override
            public boolean isCredentialsNonExpired() {
                return true;
            }

            @Override
            public boolean isEnabled() {
                return true;
            }
        }

}
