package com.cbrgenomics.exercise.services;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.time.Instant;
import java.util.Optional;

@Service
public class JwtUtils {

    public static final String USERNAME_CLAIM = "username";

    @Value("${oauth.token.secret}")
    private String secret;

    @Value("${oauth.token.issuer}")
    private String issuer;

    @Value("${oauth.token.maxAge}")
    private Long maxAge;

    public Optional<String> getUsername(String token) {
        Algorithm algorithm = Algorithm.HMAC512(secret);
        JWTVerifier verifier = JWT.require(algorithm).withIssuer(issuer).build();

        DecodedJWT jwt;
        try {
            jwt = verifier.verify(token);
        } catch (JWTVerificationException ex) {
            return Optional.empty();
        }

        return Optional.ofNullable(jwt.getClaim(USERNAME_CLAIM).asString());
    }

    public String getToken(String username) {
        Algorithm algorithm = Algorithm.HMAC512(secret);

        return JWT.create()
                .withIssuer(issuer)
                .withExpiresAt(Date.from(Instant.now().plusSeconds(maxAge)))
                .withClaim(USERNAME_CLAIM, username)
                .sign(algorithm);
    }

}
