package com.cbrgenomics.exercise.repositories;

import com.cbrgenomics.exercise.datasource.tables.pojos.News;
import org.jooq.DSLContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import java.util.stream.Stream;

import static com.cbrgenomics.exercise.datasource.Tables.NEWS;

@Repository
public class NewsRepository {

    @Autowired
    private DSLContext context;

    /**
     * Creates a new News
     * @param news
     */
    public void insert(News news) {
        context.executeInsert(context.newRecord(NEWS, news));
    }

    /**
     * gets a News by it's id
     * @param id
     * @return
     */
    public News find(Long id) {
        return context.selectFrom(NEWS).where(NEWS.ID.eq(id)).fetchOneInto(News.class);
    }

    /**
     * gets all news by the publish status
     * @param isPublished
     * @return
     */
    public Stream<News> findByPublishStatus(boolean isPublished) {
        return context.selectFrom(NEWS).where(NEWS.PUBLISHED.eq(isPublished)).fetchStreamInto(News.class);
    }

    /**
     * gets all news by author
     * @param author
     * @return
     */
    public Stream<News> findUnpublishedByAuthor(Long author) {
        return context.selectFrom(NEWS).where(NEWS.CREATED_BY.eq(author)).and(NEWS.PUBLISHED.isFalse()).fetchStreamInto(News.class);
    }

    /**
     * Updates the news, if the user matches the creator
     * @param news
     * @param id
     * @return
     */
    public int update(News news, Long id) {
        return context.executeUpdate(context.newRecord(NEWS, news), NEWS.ID.eq(id));
    }

    /**
     * Publishes a story
     * @param id
     * @return
     */
    public int publish(Long id) {
        return context.update(NEWS)
                .set(NEWS.PUBLISHED, true)
                .where(NEWS.ID.eq(id))
                .execute();
    }
    /**
     * Adds a UPVOTE counter to the specified news
     * @param id
     * @return
     */
    public int upvote(Long id) {
        return context.update(NEWS).set(NEWS.UPVOTES, NEWS.UPVOTES.plus(1))
                .where(NEWS.ID.eq(id))
                .returning(NEWS.UPVOTES)
                .fetchOne()
                .get(NEWS.UPVOTES);
    }

}
