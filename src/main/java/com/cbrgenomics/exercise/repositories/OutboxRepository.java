package com.cbrgenomics.exercise.repositories;

import com.cbrgenomics.exercise.datasource.enums.EventType;
import com.cbrgenomics.exercise.datasource.enums.InteractionType;
import com.cbrgenomics.exercise.datasource.tables.pojos.Outbox;
import org.jooq.DSLContext;
import org.jooq.impl.DSL;
import static org.jooq.impl.DSL.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.stream.Stream;

import static com.cbrgenomics.exercise.datasource.Tables.*;
import static com.cbrgenomics.exercise.datasource.tables.Outbox.OUTBOX;

@Repository
public class OutboxRepository {

    @Autowired
    private DSLContext context;

    /**
     * Deletes each notification present in the outbox table, returning them first
     * @return
     */
    public Stream<Outbox> lockAndHandleComms() {
        return context.deleteFrom(OUTBOX)
                .returning()
                .fetch()
                .into(Outbox.class)
                .stream();
    }

    /**
     * Sends a upvotes notification message to all users registered
     * This may need to be asynchronous for a large number of users
     * @param news
     */
    public void sendUpvotesNotification(Long news) {
        context.insertInto(OUTBOX)
                .columns(OUTBOX.NEWS_ID, OUTBOX.TYPE, OUTBOX.USER_ID, OUTBOX.EMITER)
                .select(DSL.select(DSL.inline(news), DSL.inline(EventType.REACHED_UPVOTES), USER.ID, USER.NOTIFICATION_TYPE).from(USER))
                .execute();
    }

    /**
     * Sends a modification notification message to all users that had read that message
     * This may need to be asynchronous for a large number of users
     * @param news
     */
    public void sendModificationNotification(Long news) {
        context.insertInto(OUTBOX)
                .columns(OUTBOX.NEWS_ID, OUTBOX.TYPE, OUTBOX.USER_ID, OUTBOX.EMITER)
                .select(select(inline(news), inline(EventType.UPDATED_NEWS), USER.ID, USER.NOTIFICATION_TYPE)
                        .from(USER)
                        .where(USER.ID.in(select(INTERACTION.INTERACTED_BY)
                                .from(INTERACTION)
                                .where(INTERACTION.TYPE.eq(InteractionType.VISUALIZATION))
                                .and(INTERACTION.NEWS_ID.eq(news)))))
                .execute();
    }



}
