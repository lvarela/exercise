package com.cbrgenomics.exercise.repositories;

import com.cbrgenomics.exercise.datasource.enums.InteractionType;
import org.jooq.DSLContext;
import static org.jooq.impl.DSL.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import static com.cbrgenomics.exercise.datasource.Tables.NEWS;
import static com.cbrgenomics.exercise.datasource.tables.Interaction.INTERACTION;

@Repository
public class InteractionRepository {

    @Autowired
    private DSLContext context;

    /**
     * Marks an interaction between a user and a news
     * It ignores two upvotes for the same user/news pair, also useful for analytics
     * @param user
     * @param news
     * @param type
     * @return
     */
    public int interact(Long user, Long news, InteractionType type) {
        return context.insertInto(INTERACTION).columns(INTERACTION.NEWS_ID, INTERACTION.INTERACTED_BY, INTERACTION.TYPE)
                //only interact with published news
                .select(select(NEWS.ID, inline(user), inline(type)).from(NEWS).where(NEWS.ID.eq(news)).and(NEWS.PUBLISHED.isTrue()))
                .execute();
    }

}
