package com.cbrgenomics.exercise.repositories;

import com.cbrgenomics.exercise.datasource.enums.NotificationType;
import com.cbrgenomics.exercise.datasource.tables.pojos.User;
import org.jooq.DSLContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Optional;

import static com.cbrgenomics.exercise.datasource.Tables.USER;

@Repository
public class UserRepository {

    @Autowired
    private DSLContext context;

    /**
     * Inserts a new user into the system
     * @param user
     */
    public void insert(User user) {
        context.executeInsert(context.newRecord(USER, user));
    }

    /**
     * updates the notification type for the given user
     * @param id
     * @param type
     */
    public void updateNotificationType(Long id, NotificationType type) {
        context.update(USER).set(USER.NOTIFICATION_TYPE, type).where(USER.ID.eq(id)).execute();
    }

    /**
     * searches the user by the username
     * @param username
     * @return
     */
    public Optional<User> searchByUsername(String username) {
        return context.selectFrom(USER).where(USER.USERNAME.equalIgnoreCase(username)).fetchOptionalInto(User.class);
    }

}
