package com.cbrgenomics.exercise.configuration;

import com.cbrgenomics.exercise.datasource.enums.Role;
import com.cbrgenomics.exercise.datasource.tables.pojos.News;
import com.cbrgenomics.exercise.datasource.tables.pojos.User;
import com.cbrgenomics.exercise.repositories.NewsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.PermissionEvaluator;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.io.Serializable;

@Component
@Transactional(readOnly=true)
/**
 * Implements the security rules and permissions for most of the rest endpoints
 */
public class CustomPermissionEvaluator implements PermissionEvaluator {

    @Autowired
    private NewsRepository newsRepo;

    @Resource(name = "currentUser")
    private User currentUser;

    @Override
    public boolean hasPermission(Authentication authentication, Object targetDomainObject, Object permission) {
        return false;
    }

    @Override
    public boolean hasPermission(Authentication authentication, Serializable targetId, String targetType, Object permission) {
        switch (targetType) {
            case "NEWS":
                return hasNewsPermissions((Long) targetId, (String) permission);
            default:
                return false;
        }
    }

    private boolean hasNewsPermissions(Long id, String permission) {
        News item = newsRepo.find(id);

        switch (permission) {
            case "READ":
                //any user may read published news, the author may read it's drafts, and admins can read all
                return item.getPublished() || currentUser.getRole() == Role.ADMIN || currentUser.getId().equals(item.getCreatedBy());
            case "WRITE":
            case "PUBLISH":
                //only the creator may write into the news item
                return currentUser.getId().equals(item.getCreatedBy());
            case "VISUALIZE":
            case "UPVOTE":
                //can only "visualize" and upvote published news
                return item.getPublished();
            default:
                return false;
        }
    }
}
