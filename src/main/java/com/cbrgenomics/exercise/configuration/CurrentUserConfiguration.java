package com.cbrgenomics.exercise.configuration;

import com.cbrgenomics.exercise.controllers.AuthenticationController;
import com.cbrgenomics.exercise.datasource.tables.pojos.User;
import com.cbrgenomics.exercise.repositories.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.context.WebApplicationContext;


@Configuration
/**
 * Provides a User bean that corresponds to the current logged in user
 */
public class CurrentUserConfiguration {

    private static final Logger LOG = LoggerFactory.getLogger(CurrentUserConfiguration.class);

    @Autowired
    private UserRepository repository;

    public String getCurrentUsername() {
        return ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername();
    }

    @Bean
    @Scope(value = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.TARGET_CLASS)
    public User currentUser() {
        LOG.info("Getting current user info");
        return repository.searchByUsername(getCurrentUsername()).get();
    }

}
