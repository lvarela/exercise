package com.cbrgenomics.exercise.notifications.emitters;

import com.cbrgenomics.exercise.datasource.enums.NotificationType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmitterFactory {

    @Autowired
    private EmailEmitter email;

    @Autowired
    private SmokeSignalEmitter smoke;

    public Emitter getEmitter(NotificationType type) {
        switch (type) {
            case EMAIL:
                return email;
            case SMOKE_SIGNALS:
                return smoke;
            default:
                throw new UnsupportedOperationException("Seems like we forgot some notification type for " + type.name());
        }
    }

}
