package com.cbrgenomics.exercise.notifications.emitters;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class SmokeSignalEmitter implements Emitter {

    public static Logger LOG = LoggerFactory.getLogger(SmokeSignalEmitter.class);

    @Override
    public void emitNotification(Long user, String text) {
        LOG.info("Smoke Signals to user #{}: {}", user, text);
    }
}
