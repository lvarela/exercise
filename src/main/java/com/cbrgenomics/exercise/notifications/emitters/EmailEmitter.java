package com.cbrgenomics.exercise.notifications.emitters;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class EmailEmitter implements Emitter {

    public static Logger LOG = LoggerFactory.getLogger(EmailEmitter.class);

    @Override
    public void emitNotification(Long user, String text) {
        LOG.info("Email to user #{}: {}", user, text);
    }
}
