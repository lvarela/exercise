package com.cbrgenomics.exercise.notifications.emitters;

public interface Emitter {

    void emitNotification(Long user, String text);

}
