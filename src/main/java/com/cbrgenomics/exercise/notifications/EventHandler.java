package com.cbrgenomics.exercise.notifications;

import com.cbrgenomics.exercise.datasource.enums.EventType;
import com.cbrgenomics.exercise.notifications.emitters.EmitterFactory;
import com.cbrgenomics.exercise.notifications.generators.NotificationGeneratorFactory;
import com.cbrgenomics.exercise.repositories.OutboxRepository;
import com.cbrgenomics.exercise.repositories.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.concurrent.TimeUnit;

@Component
public class EventHandler {

    @Autowired
    private OutboxRepository outbox;

    @Autowired
    private UserRepository userRepo;

    @Autowired
    private EmitterFactory emiter;

    @Autowired
    private NotificationGeneratorFactory generator;

    private static final Logger LOG = LoggerFactory.getLogger(EventHandler.class);

    /**
     * This function can be made as complex as needed.
     * The most "urgent" improvement would be to add producer idempotency to each of the emitters
     *
     * The pattern name is called transactional outbox
     */
    @Scheduled(fixedDelay = 5, timeUnit = TimeUnit.SECONDS)
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = { Throwable.class })
    public void handle() {
        outbox.lockAndHandleComms().forEach(o -> {
            try {
                emiter.getEmitter(o.getEmiter())
                        .emitNotification(o.getUserId(), generateNotificationText(o.getType(), o.getNewsId()));
            } catch (Exception e) {
                // we want to catch any exception here to avoid trying to send "poison messages"
                // ie, messages that always fail, causing the transaction to rollback and infinite retries
                // we should use something like a deadletter queue
                // or backoff a limited number of retries on individual notifications
                LOG.error("Could not send message to user={}, news={}", o.getUserId(), o.getNewsId(), e);
            }
        });
    }

    private String generateNotificationText(EventType type, Long news) {
        return generator.get(type).generateText(news);
    }

}
