package com.cbrgenomics.exercise.notifications.generators;

import org.springframework.stereotype.Component;

@Component
public class NewsUpdated implements NotificationGenerator {

    @Override
    public String generateText(Long news) {
        return String.format("Reddit clone has updated the news #%d", news);
    }
}
