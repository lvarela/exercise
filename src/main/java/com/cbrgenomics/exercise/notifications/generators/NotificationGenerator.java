package com.cbrgenomics.exercise.notifications.generators;

public interface NotificationGenerator {

    String generateText(Long news);

}
