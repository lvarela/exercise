package com.cbrgenomics.exercise.notifications.generators;

import com.cbrgenomics.exercise.datasource.enums.EventType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class NotificationGeneratorFactory {

    @Autowired
    private NewsUpdated updates;

    @Autowired
    private UpvotesReached upvotes;

    public NotificationGenerator get(EventType type) {
        switch(type) {
            case REACHED_UPVOTES:
                return upvotes;
            case UPDATED_NEWS:
                return updates;
            default:
                throw new UnsupportedOperationException("Unsupported event type: " + type.name());
        }
    }

}
