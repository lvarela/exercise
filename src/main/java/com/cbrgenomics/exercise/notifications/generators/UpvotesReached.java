package com.cbrgenomics.exercise.notifications.generators;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class UpvotesReached implements NotificationGenerator {

    @Value("${cbrgenomics.upvotes.threshold}")
    private Integer threshold;

    @Override
    public String generateText(Long news) {
        return String.format("Reddit clone has reached %d upvotes on the news #%d", threshold, news);
    }
}
